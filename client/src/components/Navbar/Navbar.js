import React from 'react'
import memories from '../../images/memories.png';
import { AppBar, Typography } from '@material-ui/core';

import styles from './styles';


const Navbar = () => {
  const classes = styles();

  return (
    <AppBar className={classes.appBar} position="static" color="inherit">
        <Typography className={classes.heading} variant="h2" align="center">
          Memories
        </Typography>
        <img className={classes.image} src={memories} alt="memories" height="60" />
  </AppBar>
    )
}

export default Navbar